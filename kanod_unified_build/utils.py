#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


"""
Several utilities for dealing with yaml files import and validation
"""
from jsonschema import validate, ValidationError
import sys
import time
import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


def eprint(msg):
    sys.stderr.write(msg)
    sys.stderr.write('\n')
    sys.stderr.flush()


def retry_backoff(retries, msg, fn, *args):
    count = 0
    delay = 1
    while True:
        try:
            return fn(*args)
        except Exception:
            count += 1
            delay *= 2
            if count == retries:
                raise
            else:
                eprint(f'{msg}: Iteration {count}')
                time.sleep(delay)


def treat_exception(message: str, exit_code: int):
    """Unified treatment of exceptions

    :param message: Message to display
    :param exit_code: script exit code
    :return: None
    """
    eprint(f'Error: {message}')
    sys.exit(exit_code)


def yaml2obj(inputfd):
    """Load a yaml file from a file descriptor

    :param inputfd: input file descriptor
    :return: object corresponding to yaml file
    """
    try:
        result = yaml.load(inputfd, Loader=Loader)
    except yaml.parser.ParserError as e:
        treat_exception(f'Yaml loading exited with error: {e}', 3)
    return result


def loadyaml(filename):
    """Load a yaml file

    :param filename: name of the file
    :return: object instance corresponding to the yaml file
    """
    try:
        with open(filename) as fd:
            result = yaml2obj(fd)
    except FileNotFoundError as e:
        treat_exception(f'Yaml file was not found due to error: {e}', 4)
    return result


def check_pipeline_data_structure(obj):
    """Check configuration file against schema

    exits in case of validation errors
    :param obj: object to check
    :return: None
    """
    schema = {
        "type": "object",
        "properties": {
            "variables": {
                "type": "object",
                "patternProperties": {
                    "^.*$": {
                        "type": "string"
                    }
                }
            },
            "propagate_env_variables": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            },
            "jobs": {
                "type": "array",
                "items": {
                    "type": "object",
                    "required": ["pipeline"],
                    "properties": {
                        "pipeline": {
                            "type": "string"
                        },
                        "artifact-check": {
                            "type": "string"
                        },
                        "tags": {
                            "oneOf": [
                                {
                                    "type": "array",
                                    "items": {
                                        "type": "string"
                                    }
                                },
                                {
                                    "type": "string"
                                }
                            ]
                        },
                        "variables": {
                            "type": "object",
                            "patternProperties": {
                                "^.*$": {
                                    "oneOf": [
                                        {"type": "string"},
                                        {
                                            "type": "array",
                                            "items": {
                                                "type": "string"
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    try:
        validate(obj, schema)
    except ValidationError as e:
        treat_exception(
            f'Error validating pipelines file against schema: {e}', 10)


if __name__ == '__main__':
    pass
