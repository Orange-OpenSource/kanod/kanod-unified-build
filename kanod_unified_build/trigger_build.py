#!/usr/bin/env python

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import argparse
import gitlab
from itertools import product
import os
import requests
import time
import urllib3

from kanod_unified_build.dictsubst import dictsubst
from kanod_unified_build import git_utils
from kanod_unified_build.gitlab_utils import Repository
from kanod_unified_build.utils import \
    check_pipeline_data_structure, loadyaml, eprint

urllib3.disable_warnings()
GITLAB_URL = os.getenv('CI_SERVER_URL')
TOKEN = os.getenv('AUTH_TOKEN')


def check_artifacts(job, global_variables, force_sync):
    """Check if a job has all artifacts in Maven

    :param global_variables:
    :param job: Job definition according to specs
    :return: a list of variables/tags tuples coupled with a
    boolean indicating the presence of artifact
    """
    result = []
    for variables, tag in variable_cross_product(job):
        all_variables = dictsubst(global_variables)
        all_variables.update(variables)
        all_variables.update({'TAG': tag})
        if force_sync or 'artifact-check' not in job:
            sync_needed = True
        else:
            url = all_variables.substitute(job['artifact-check'])
            sync_needed = not check_url(url)
        result.append(((all_variables, tag), sync_needed))

    return result


def check_url(url: str) -> bool:
    """Check if url exists

    :param url: full url
    :return: True if request ok
    """
    eprint(f'  Checking url {url}')
    try:
        r = requests.head(url, verify=False)
    except requests.exceptions.ConnectionError as e:
        eprint(
            f'  Warning: Cannot connect to {url} due to error: {e}\n')

        return False
    except requests.exceptions.MissingSchema as e:
        eprint(
            f'  Warning: Cannot process url due to missing schema: {e}\n')
        # If we can't access the url we treat it like an existing one
        return False
    return r.ok


def partial_dictionaries(input_dictionary):
    """Generate partial dictionaries from a dictionary with lists

    Generates a cross product with dictionaries having only single elements not
    lists.
    :param input_dictionary: Initial dictionary
    :return: a list of dictionaries with simple values (instead of lists)
    the return list should contain all combinations
    """
    listified = [[
        (k, v) for v in lv] if isinstance(lv, list) else [(k, lv)]
        for k, lv in input_dictionary.items()]
    return [{k: v for (k, v) in e} for e in product(*listified)]


def filter_dictionary(input_dictionary, key_filter):
    """Filter a dictionary using a keys list

    The function returns a dictionary with keys from keys list
    :param input_dictionary: dictionary to filter
    :param key_filter: a list of keys
    :return: a filtered dictionary
    """
    return {k: v for k, v in input_dictionary.items() if
            k in key_filter}


def variable_cross_product(job):
    """Generate cross products based on variables and tags

    only variables extrapolated in artifact_check are taken into account.
    :param job: one job definition
    :return: a list of job instances (each job might generate
        several job instances)
    """
    if 'tags' in job:
        if isinstance(job['tags'], list):
            tags = job['tags']
        else:
            tags = [job['tags']]
    else:
        tags = ['master']
    return product(partial_dictionaries(job.get('variables', {})), tags)


def gitlab_auth():
    """Create a gitlab python client session

    :return: gitlab object instance
    """
    gl = gitlab.Gitlab(
        GITLAB_URL, private_token=TOKEN,
        ssl_verify=False, per_page=100)
    return gl


def trigger_pipeline(variables_and_tag, repository, failures):
    """Trigger a gitlab pipeline run

    :param variables_and_tag: variables and tag used in triggering the pipeline
    :param repository: repository instance
    :return: None
    """
    trigger = repository.trigger
    variables, tag = variables_and_tag
    retry = 0
    try:
        pipeline = repository.project.trigger_pipeline(
            tag, trigger.token, variables=variables)
    except gitlab.exceptions.GitlabCreateError as e:
        eprint('-' * 80)
        eprint(
            f'Triggering pipeline for project {repository.project.name} '
            f'and tag {tag} failed with error: {e}')
        reason = (repository.project.name, 'Triggering pipeline',
                  variables_and_tag)
        failures.append(reason)
        return
    while retry < 3600:
        if pipeline.finished_at is None:
            pipeline.refresh()
            retry += 1
            time.sleep(1)
        else:
            break
    else:
        reason = (
            repository.project.name,
            'Project timeout',
            variables_and_tag
        )
        failures.append(reason)
        eprint('-' * 80)
        eprint(
            f'Triggering pipeline for project {repository.project.name} '
            f'timed out')


def build_project_locally(build_dir, project, varstag, options, failures):
    """builds a project in the contex of kanod-unified-builder

    Invokes the build.sh script that should be in each project
    :param buld_dir: where to build the project.
    :param project: the project subpath
    :param varstag: a variable dictionary and a tag
    :param options: call options to check if it is only fetch or build
    """
    (variables, tag) = varstag
    if (variables.get('TAG', None) is not None and
            variables.get('VERSION', None) is None):
        variables['VERSION'] = variables['TAG']

    try:
        repo = git_utils.fetch_project(
            build_dir, project, variables['GIT_URL'],
            not options.build_only)
    except Exception:
        eprint(f'Error: Cloning project {project} failed')
        reason = (project, 'Cloning', variables)
        failures.append(reason)
        return
    if options.fetch_only:
        return
    try:
        git_utils.fetch_tag(repo, tag)
    except Exception:
        eprint('-' * 80)
        eprint(f'Error: Checking out {tag} for {project} failed')
        reason = (project, 'Fetching', variables)
        failures.append(reason)
        return
    if not git_utils.build_project(build_dir, project, variables):
        eprint('-' * 80)
        eprint(f'Error: Building project {project} failed')
        reason = (project, 'Building', variables)
        failures.append(reason)
    else:
        eprint('-' * 80)
        eprint(f'Successful project build for {project}')


def main():
    """Main function

    :return: None
    """
    parser = argparse.ArgumentParser(
        description='Centralized pipeline trigger for kanod')
    parser.add_argument(
        '--pipelines', required=True,
        help='Name of yaml file containing all kanod repo with pipelines')
    parser.add_argument(
        '--force-sync', action='store_true',
        help='Force sync of artefacts ignoring the result check'
    )
    parser.add_argument(
        '--build-dir',
        help='Name of a build dir for local build without gitlab')
    parser.add_argument(
        '-d', '--debug', action='store_true',
        help='Enable debug (deprecated)')
    parser.add_argument(
        '--fetch-only', action='store_true',
        help='Only fetch the repository')
    parser.add_argument(
        '--build-only', action='store_true',
        help='Only build the repository')
    options = parser.parse_args()
    pipelines_input_file = options.pipelines
    build_dir = options.build_dir
    force_sync = options.force_sync
    pipelines = loadyaml(pipelines_input_file)
    check_pipeline_data_structure(pipelines)
    # Precedence order: propagated environment variables (CI/CD variables),
    # global variables in configuration,
    # pipeline specific variables in configuration

    env_filter = pipelines.get('propagate_env_variables', [])
    not_found_env_vars = set(env_filter).difference(set(os.environ.keys()))
    if not_found_env_vars:
        eprint(
            "Warning: Cannot find the following variables in the "
            f"current environment: {','.join(not_found_env_vars)}")
    global_variables = filter_dictionary(os.environ, env_filter)
    global_variables.update(pipelines['variables'])
    jobs = pipelines['jobs']
    if build_dir is None:
        gl = gitlab_auth()
    else:
        git_utils.global_config(build_dir, global_variables)

    failures = []
    for job in jobs:

        url = job['pipeline']

        eprint('')
        eprint('*' * 80)
        eprint(f'Pipeline {url}')
        eprint('*' * 80)

        if build_dir is None:
            repository = Repository(url, gl)
            if not repository.project:
                eprint(
                    f'Error: Cannot find project corresponding to url {url}')
                continue
            if not repository.trigger:
                eprint(
                    'Error: Cannot trigger pipeline for project '
                    f'{repository.project.name} due to permission errors')
                continue

        artifacts_need_sync = check_artifacts(
            job, global_variables, force_sync)

        if not artifacts_need_sync:
            continue

        for variables_tag_dict, need_sync in artifacts_need_sync:
            if need_sync:
                eprint(f"Triggering pipeline for project {url}")
                eprint('-' * 80)
                if build_dir is None:
                    trigger_pipeline(
                        variables_tag_dict, repository, failures)
                else:
                    build_project_locally(
                        build_dir, url, variables_tag_dict,
                        options, failures)
            else:
                eprint(f"Project {url} has the artifact")
            eprint('*' * 80)
            eprint('')
    if len(failures) > 0:
        eprint('*' * 80)
        eprint('FAILURE SUMMARY')
        eprint('*' * 80)
        for (url, cause, vars) in failures:
            eprint(f'Job {url} failed: {cause}')
            for (name, val) in vars.items():
                eprint(f'    {name}: {val}')
        exit(1)


if __name__ == '__main__':
    main()
