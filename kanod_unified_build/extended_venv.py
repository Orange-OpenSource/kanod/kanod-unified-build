#
# Copyright (C) 2013-2020 Vinay Sajip. New BSD License.
#
# Modfied for robustness against transient network errors.

import os
import os.path
from subprocess import Popen, PIPE
import sys
from threading import Thread
from urllib.parse import urlparse
from urllib.request import urlretrieve
import venv

from kanod_unified_build import utils


class ExtendedEnvBuilder(venv.EnvBuilder):
    """Builder installing setuptools and pip

    This builder installs setuptools and pip so that you can pip or
    easy_install other packages into the created environment.

    :param nodist: If True, setuptools and pip are not installed into the
                   created environment.
    :param nopip: If True, pip is not installed into the created
                  environment.
    :param progress: If setuptools or pip are installed, the progress of the
                     installation can be monitored by passing a progress
                     callable. If specified, it is called with two
                     arguments: a string indicating some progress, and a
                     context indicating where the string is coming from.
                     The context argument can have one of three values:
                     'main', indicating that it is called from virtualize()
                     itself, and 'stdout' and 'stderr', which are obtained
                     by reading lines from the output streams of a subprocess
                     which is used to install the app.

                     If a callable is not specified, default progress
                     information is output to sys.stderr.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.verbose = False

    def post_setup(self, context):
        """Set up any packages which need to be pre-installed

        :param context: The information for the environment creation request
                        being processed.
        """
        os.environ['VIRTUAL_ENV'] = context.env_dir
        self.install_setuptools(context)
        # Can't install pip without setuptools
        self.install_pip(context)
        self.upgrade_setuptools(context)

    def reader(self, stream, context):
        """Read lines from a subprocess' output stream

        and either pass to a progress
        callable (if specified) or write progress information to sys.stderr.
        """
        while True:
            s = stream.readline()
            if not s:
                break
            if not self.verbose:
                sys.stderr.write('.')
            else:
                sys.stderr.write(s.decode('utf-8'))
            sys.stderr.flush()
        stream.close()

    def install_script(self, context, name, url):
        """Tries to fetch and run an installation script.

        :param context: The information for the environment creation request
                        being processed.
        :param name: common name of the script
        :param url: url of the script
        """
        _, _, path, _, _, _ = urlparse(url)
        fn = os.path.split(path)[-1]
        binpath = context.bin_path
        distpath = os.path.join(binpath, fn)
        # Download script into the env's binaries folder
        utils.retry_backoff(5, f'install {name}', urlretrieve, url, distpath)
        if self.verbose:
            term = '\n'
        else:
            term = ''
        sys.stderr.write('Installing %s ...%s' % (name, term))
        sys.stderr.flush()
        # Install in the env
        args = [context.env_exe, fn]
        p = Popen(args, stdout=PIPE, stderr=PIPE, cwd=binpath)
        self.feed_back(p)
        # Clean up - no longer needed
        os.unlink(distpath)

    def upgrade_setuptools(self, context):
        """Upgrade setuptools after pip install"""
        binpath = context.bin_path
        pippath = os.path.join(binpath, 'pip')
        sys.stderr.write('uninstall setuptools')
        args = [pippath, 'uninstall', 'setuptools', '-y']
        p = Popen(args, stdout=PIPE, stderr=PIPE, cwd=binpath)
        self.feed_back(p)
        sys.stderr.write('install setuptools')
        args = [pippath, 'install', 'setuptools']
        p = Popen(args, stdout=PIPE, stderr=PIPE, cwd=binpath)
        self.feed_back(p)

    def feed_back(self, p):
        """Writes back process output to the common stream"""
        t1 = Thread(target=self.reader, args=(p.stdout, 'stdout'))
        t1.start()
        t2 = Thread(target=self.reader, args=(p.stderr, 'stderr'))
        t2.start()
        p.wait()
        t1.join()
        t2.join()
        sys.stderr.write('done.\n')

    def install_setuptools(self, context):
        """Install setuptools in the environment.

        :param context: The information for the environment creation request
                        being processed.
        """
        url = 'https://bootstrap.pypa.io/ez_setup.py'
        self.install_script(context, 'setuptools', url)
        # clear up the setuptools archive which gets downloaded

        def pred(o):
            o.startswith('setuptools-') and o.endswith('.tar.gz')  # noqa: E731

        files = filter(pred, os.listdir(context.bin_path))
        for f in files:
            f = os.path.join(context.bin_path, f)
            os.unlink(f)

    def install_pip(self, context):
        """Install pip in the environment.

        :param context: The information for the environment creation request
                        being processed.
        """
        url = 'https://bootstrap.pypa.io/get-pip.py'
        self.install_script(context, 'pip', url)

# Truncated from original for our needs.
