#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


"""
Module implementing a repository class
"""
import gitlab


class Repository:
    """object knowing how to deal with git repositories

    we should check if gitlab credentials are for the same gitlab url
    as the repo gl should be a class attribute.
    """

    def __init__(self, fullname, gitlab_connection):
        """Class initialization

        :param fullname: full path of the repository with groups
        :param gitlab_connection: gitlab python client instance
        """
        self.gl = gitlab_connection
        self.fullname = fullname
        self.project = next(
            (project
             for project in self.gl.projects.list(all=True)
             if fullname == project.path_with_namespace),
            None)
        self.trigger = None
        if not self.project:
            return
        trigger_description = 'Automated trigger'
        for trigger in self.project.triggers.list():
            if trigger.description == trigger_description:
                self.trigger = trigger
                return
        try:
            self.trigger = self.project.triggers.create(
                {'description': trigger_description})
        except gitlab.exceptions.GitlabCreateError:
            self.trigger = None
