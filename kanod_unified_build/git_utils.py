#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import git
import os
from os import path
import pkgutil
import stat
import subprocess

from kanod_unified_build.extended_venv import ExtendedEnvBuilder
from kanod_unified_build import utils


def get_project_name(project):
    "Extract project name from git path"

    base = path.basename(project)
    return base[:-4] if base.endswith('.git') else base


def get_project_url(project, url):
    "Return project url "

    if project.startswith("http://") or project.startswith("https://"):
        return project
    else:
        return path.join(url, project)


def fetch_project(build_dir, project, url, sync):
    """Fetch project from git

    :param build_dir: folder where all the project are fetched
    :param project: path to the project on git repo
    :param url: url of git repo
    :sync: boolean whether to sync or not
    :return: data structure describing the repository
    """
    project_name = get_project_name(project)
    target = path.abspath(path.join(build_dir, project_name))
    if path.isdir(target):
        repo = git.Repo(target)
        if sync:
            inst = repo.remote('origin')
            utils.retry_backoff(6, f'fetching {project_name}', inst.fetch)
    elif sync:
        repo = utils.retry_backoff(
            6, f'cloning {project_name}',
            git.Repo.clone_from, get_project_url(project, url), target)
    else:
        utils.eprint(f'Error: Project {project} is not provisionned.')
        raise Exception('no project dir cannot proceed.')
    return repo


def fetch_tag(repo, tag):
    """Fetch a given tag

    It will fetch a build.sh on the master branch if there is none in the tag
    commit.
    :param repo: the python git repo data structure
    :param tag: a string defining the tag
    """
    repo.head.reset('--hard')
    repo.git.checkout(tag)

    if tag == 'master':
        utils.retry_backoff(5, 'pulling master', repo.remote('origin').pull)
    build_path = path.join(repo.working_dir, 'build.sh')
    if not path.exists(build_path):
        try:
            repo.git.checkout('master', '--', 'build.sh')
        except git.exc.GitCommandError:
            utils.eprint('  Warning: No build.sh found for repo')
            return
    os.chmod(build_path, os.stat(build_path).st_mode | stat.S_IEXEC)


def venv_config(build_dir):
    """Configure a virtual environment

    Populate it with setuptools and pip.
    :param build_dir: where all projects are built
    :return: path to the virtual environment.
    """

    venv_dir = path.abspath(path.join(build_dir, '.venv'))
    if not path.isdir(venv_dir):
        builder = ExtendedEnvBuilder()
        builder.create(venv_dir)
    return venv_dir


def maven_config(build_dir, variables):
    """Setup a local maven configuration from project variables.

    :param build_dir: where all projects are built
    :variables: dictionnary of variables. Used to find maven required config
    :return: path to maven folder.
    """
    maven_dir = path.abspath(path.join(build_dir, '.m2'))
    if not path.exists(maven_dir):
        data = pkgutil.get_data(__name__, 'settings.tmpl')
        substituted = str(data, encoding='utf-8').format(
            no_proxy=variables['RUNNER_PROXY_NO_PROXY'],
            proxy_port=variables['RUNNER_PROXY_PORT'],
            proxy_host=variables['RUNNER_PROXY_HOST'],
            proxy_active=variables['RUNNER_PROXY_ACTIVE'],
            admin_user=variables['NEXUS_ADMIN_USER'],
            admin_password=variables['NEXUS_ADMIN_PASSWORD']
        )
        os.makedirs(maven_dir)
        os.makedirs(path.join(maven_dir, 'repository'))
        with open(path.join(maven_dir, 'settings.xml'), 'w') as writer:
            writer.write(substituted)
    return maven_dir


def global_config(build_dir, vars):
    maven_dir = maven_config(build_dir, vars)
    venv_dir = venv_config(build_dir)
    vars['MAVEN_CLI_OPTS'] = f'-s {maven_dir}/settings.xml --batch-mode'
    vars['MAVEN_OPTS'] = f'-Dmaven.repo.local={maven_dir}/repository'
    vars['KANOD_VENV'] = venv_dir
    if vars.get('http_proxy', None) is not None:
        vars['HTTP_PROXY'] = vars['http_proxy']
    if vars.get('https_proxy', None) is not None:
        vars['HTTPS_PROXY'] = vars['https_proxy']
    if vars.get('no_proxy', None) is not None:
        vars['NO_PROXY'] = vars['no_proxy']
    # The following line is for compatibility only with old scripts
    vars['IMAGE_SYNC_DIR'] = '-'


def build_project(build_dir, project, variables):
    project_name = get_project_name(project)
    dir = path.abspath(path.join(build_dir, project_name))
    ret = subprocess.run('./build.sh', cwd=dir, env=variables)
    if (ret.returncode != 0):
        utils.eprint(
            f'build_project: problem with {project_name}: {ret.returncode}')
        return False
    return True
