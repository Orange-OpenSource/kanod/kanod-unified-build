#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import unittest

from kanod_unified_build import dictsubst


class TestDictsubst(unittest.TestCase):
    '''Basic unit test on DictSubst module'''

    def test_substitute(self):
        dict = {"x": "val1", "y": "val2"}
        ds = dictsubst.dictsubst(dict)
        result = ds.substitute("*** ${x} ***")
        self.assertEqual(result, "*** val1 ***")
        result = ds.substitute("*** $y ***")
        self.assertEqual(result, "*** val2 ***")

    def test_substitute_repeated(self):
        dict = {"x": "val1", "y": "val2"}
        ds = dictsubst.dictsubst(dict)
        result = ds.substitute("*** ${x} *** ${y} *** ${x} ***")
        self.assertEqual("*** val1 *** val2 *** val1 ***", result)

    def test_update(self):
        dict1 = {"x1": "v1", "x2": "v2"}
        dict2 = {"y1": "v3", "y2": "v4", "x2": "v7"}
        dict3 = {"z1": "v5", "z2": "v6"}
        ds = dictsubst.dictsubst(dict1)
        ds.update(dict2)
        ds.update(dict3)
        result = ds.substitute("$x1 $y1 $z1 $x2")
        self.assertEqual("v1 v3 v5 v7", result)
