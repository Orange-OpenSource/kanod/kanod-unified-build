===================
Kanod Unified Build
===================

Kanod Unified Build provides a way to build a set of git projects providing
artifacts to a registry using a YAML manifest to describe:

* The tasks to perform
* A way to check if an artefact is available
* Shell variables definitions that are used to specialize the tasks

Kanod Unified Build was designed to trigger gitlab pipelines but it can
also be run independently as long as each git project provides a suitable
``build.sh`` script to create and upload the artefact.

.. jsonschema:: schema.yaml
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:


Usage without CI
================
The utility can be recompiled with 

.. code-block:: shell

    python3 setup.py install --user

Then it can be used as:

.. code-block:: shell

    kanod-trigger-build --pipelines config.yaml --build-dir /temp/dir [options]


..option:: -h, --help

    show the help message and exit

..option:: --pipelines PIPELINES

    Name of yaml file containing all kanod repo with
    pipelines

..option:: --force-sync

    Force sync of artefacts ignoring the result check
    of ``artifact-check``

..option:: --build-dir BUILD_DIR

    Path of a build folder for local build without gitlab

..option:: -d, --debug

    Enable debug (deprecated)

..option:: --fetch-only

    Only fetch the repository

..option:: --build-only
    Only perform the build step assuming the repository is fetched.
    It will still checkout the relevant tag.

Usage with CI
=============

The docker image must be launched as a gitlab pipeline on the Gitlab instance
hosting all the projects. It inherits the following variables:

``CI_SERVER_URL``
    The URL of the gitlab.

``AUTH_TOKEN``
    The token used to authenticate against gitlab API and create new pipelines
